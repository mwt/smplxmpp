# configuration options
option(SMPLXMPP_GLOOX_USE_SYSTEM "iff ON, use the gloox version supplied by the system" ON)

set(SMPLXMPP_GLOOX_SRC_PATH "https://camaya.net/download/gloox-1.0.24.tar.bz2" CACHE STRING "ONLY USED IF SMPLXMPP_GLOOX_USE_SYSTEM is OFF: path (file or URL) to gloox source (may be a tarball)")
set(SMPLXMPP_GLOOX_SYSTEM_LINKER_FLAG "-lgloox" CACHE STRING "ONLY USED IF SMPLXMPP_GLOOX_USE_SYSTEM is ON: linker flag for system gloox")

# always use the target "gloox"
add_library(gloox INTERFACE)

if (SMPLXMPP_GLOOX_USE_SYSTEM)
    # link against system-provided gloox
    target_link_libraries(gloox INTERFACE
        "${SMPLXMPP_GLOOX_SYSTEM_LINKER_FLAG}"
        )
else()
    # download gloox src and build ourselves
    include(ExternalProject)
    ExternalProject_Add(gloox_external
        PREFIX "${PROJECT_BINARY_DIR}/extern/gloox_external"
        URL "${SMPLXMPP_GLOOX_SRC_PATH}"
        CONFIGURE_COMMAND "${CMAKE_CURRENT_BINARY_DIR}/extern/gloox_external/src/gloox_external/configure" "--without-tests" "--without-examples" "--prefix" "${CMAKE_CURRENT_BINARY_DIR}/extern/gloox_external"
        )

    add_dependencies(gloox
        gloox_external
        )

    target_include_directories(gloox INTERFACE
        "${PROJECT_BINARY_DIR}/extern/gloox_external/include/"
        )

    target_link_libraries(gloox INTERFACE
        "${PROJECT_BINARY_DIR}/extern/gloox_external/lib/libgloox.a"
        )
endif()

# other gloox dependencies
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

target_link_libraries(gloox INTERFACE
    Threads::Threads
    z
    resolv
    idn
    gnutls
    ${CMAKE_DL_LIBS}
    )

