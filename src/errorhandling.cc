#include <errorhandling.h>

#include <string>
#include <gloox/gloox.h>

using namespace gloox;
using namespace std;

string glooxErrorToText(ConnectionError e) {
    switch (e) {
        case ConnectionError::ConnNoError: return "ConnNoError";
        case ConnectionError::ConnStreamError: return "ConnStreamError";
        case ConnectionError::ConnStreamVersionError: return "ConnStreamVersionError";
        case ConnectionError::ConnStreamClosed: return "ConnStreamClosed";
        case ConnectionError::ConnProxyAuthRequired: return "ConnProxyAuthRequired";
        case ConnectionError::ConnProxyAuthFailed: return "ConnProxyAuthFailed";
        case ConnectionError::ConnProxyNoSupportedAuth: return "ConnProxyNoSupportedAuth";
        case ConnectionError::ConnIoError: return "ConnIoError";
        case ConnectionError::ConnParseError: return "ConnParseError";
        case ConnectionError::ConnConnectionRefused: return "ConnConnectionRefused";
        case ConnectionError::ConnDnsError: return "ConnDnsError";
        case ConnectionError::ConnOutOfMemory: return "ConnOutOfMemory";
        case ConnectionError::ConnNoSupportedAuth: return "ConnNoSupportedAuth";
        case ConnectionError::ConnTlsFailed: return "ConnTlsFailed";
        case ConnectionError::ConnTlsNotAvailable: return "ConnTlsNotAvailable";
        case ConnectionError::ConnCompressionFailed: return "ConnCompressionFailed";
        case ConnectionError::ConnAuthenticationFailed: return "ConnAuthenticationFailed";
        case ConnectionError::ConnUserDisconnected: return "ConnUserDisconnected";
        case ConnectionError::ConnNotConnected: return "ConnNotConnected";
    }

    return "unknown error";
}

string glooxErrorToText(StreamError e) {
    switch (e) {
        case StreamError::StreamErrorBadFormat: return "StreamErrorBadFormat";
        case StreamError::StreamErrorBadNamespacePrefix: return "StreamErrorBadNamespacePrefix";
        case StreamError::StreamErrorConflict: return "StreamErrorConflict";
        case StreamError::StreamErrorConnectionTimeout: return "StreamErrorConnectionTimeout";
        case StreamError::StreamErrorHostGone: return "StreamErrorHostGone";
        case StreamError::StreamErrorHostUnknown: return "StreamErrorHostUnknown";
        case StreamError::StreamErrorImproperAddressing: return "StreamErrorImproperAddressing";
        case StreamError::StreamErrorInternalServerError: return "StreamErrorInternalServerError";
        case StreamError::StreamErrorInvalidFrom: return "StreamErrorInvalidFrom";
        case StreamError::StreamErrorInvalidId: return "StreamErrorInvalidId";
        case StreamError::StreamErrorInvalidNamespace: return "StreamErrorInvalidNamespace";
        case StreamError::StreamErrorInvalidXml: return "StreamErrorInvalidXml";
        case StreamError::StreamErrorNotAuthorized: return "StreamErrorNotAuthorized";
        case StreamError::StreamErrorPolicyViolation: return "StreamErrorPolicyViolation";
        case StreamError::StreamErrorRemoteConnectionFailed: return "StreamErrorRemoteConnectionFailed";
        case StreamError::StreamErrorResourceConstraint: return "StreamErrorResourceConstraint";
        case StreamError::StreamErrorRestrictedXml: return "StreamErrorRestrictedXml";
        case StreamError::StreamErrorSeeOtherHost: return "StreamErrorSeeOtherHost";
        case StreamError::StreamErrorSystemShutdown: return "StreamErrorSystemShutdown";
        case StreamError::StreamErrorUndefinedCondition: return "StreamErrorUndefinedCondition";
        case StreamError::StreamErrorUnsupportedEncoding: return "StreamErrorUnsupportedEncoding";
        case StreamError::StreamErrorUnsupportedStanzaType: return "StreamErrorUnsupportedStanzaType";
        case StreamError::StreamErrorUnsupportedVersion: return "StreamErrorUnsupportedVersion";
        case StreamError::StreamErrorXmlNotWellFormed: return "StreamErrorXmlNotWellFormed";
        case StreamError::StreamErrorUndefined: return "StreamErrorUndefined";
    }
    
    return "unknown error";
}

string glooxErrorToText(StanzaErrorType e) {
    switch (e) {
        case StanzaErrorType::StanzaErrorTypeAuth: return "tanzaErrorTypeAuth";
        case StanzaErrorType::StanzaErrorTypeCancel: return "StanzaErrorTypeCancel";
        case StanzaErrorType::StanzaErrorTypeContinue: return "StanzaErrorTypeContinue";
        case StanzaErrorType::StanzaErrorTypeModify: return "StanzaErrorTypeModify";
        case StanzaErrorType::StanzaErrorTypeWait: return "StanzaErrorTypeWait";
        case StanzaErrorType::StanzaErrorTypeUndefined: return "StanzaErrorTypeUndefined";
    }

    return "unknown error";
}

string glooxErrorToText(StanzaError e) {
    switch (e) {
        case StanzaErrorBadRequest: return "StanzaErrorBadRequest";
        case StanzaErrorConflict: return "StanzaErrorConflict";
        case StanzaErrorFeatureNotImplemented: return "StanzaErrorFeatureNotImplemented";
        case StanzaErrorForbidden: return "StanzaErrorForbidden";
        case StanzaErrorGone: return "StanzaErrorGone";
        case StanzaErrorInternalServerError: return "StanzaErrorInternalServerError";
        case StanzaErrorItemNotFound: return "StanzaErrorItemNotFound";
        case StanzaErrorJidMalformed: return "StanzaErrorJidMalformed";
        case StanzaErrorNotAcceptable: return "StanzaErrorNotAcceptable";
        case StanzaErrorNotAllowed: return "StanzaErrorNotAllowed";
        case StanzaErrorNotAuthorized: return "StanzaErrorNotAuthorized";
        case StanzaErrorNotModified: return "StanzaErrorNotModified";
        case StanzaErrorPaymentRequired: return "StanzaErrorPaymentRequired";
        case StanzaErrorRecipientUnavailable: return "StanzaErrorRecipientUnavailable";
        case StanzaErrorRedirect: return "StanzaErrorRedirect";
        case StanzaErrorRegistrationRequired: return "StanzaErrorRegistrationRequired";
        case StanzaErrorRemoteServerNotFound: return "StanzaErrorRemoteServerNotFound";
        case StanzaErrorRemoteServerTimeout: return "StanzaErrorRemoteServerTimeout";
        case StanzaErrorResourceConstraint: return "StanzaErrorResourceConstraint";
        case StanzaErrorServiceUnavailable: return "StanzaErrorServiceUnavailable";
        case StanzaErrorSubscribtionRequired: return "StanzaErrorSubscribtionRequired";
        case StanzaErrorUndefinedCondition: return "StanzaErrorUndefinedCondition";
        case StanzaErrorUnexpectedRequest: return "StanzaErrorUnexpectedRequest";
        case StanzaErrorUnknownSender: return "StanzaErrorUnknownSender";
        case StanzaErrorUndefined: return "StanzaErrorUndefined";
    }

    return "unknown error";
}

string glooxErrorToText(AuthenticationError e) {
    switch (e) {
        case AuthenticationError::AuthErrorUndefined: return "AuthErrorUndefined";
        case AuthenticationError::SaslAborted: return "SaslAborted";
        case AuthenticationError::SaslIncorrectEncoding: return "SaslIncorrectEncoding";
        case AuthenticationError::SaslInvalidAuthzid: return "SaslInvalidAuthzid";
        case AuthenticationError::SaslInvalidMechanism: return "SaslInvalidMechanism";
        case AuthenticationError::SaslMalformedRequest: return "SaslMalformedRequest";
        case AuthenticationError::SaslMechanismTooWeak: return "SaslMechanismTooWeak";
        case AuthenticationError::SaslNotAuthorized: return "SaslNotAuthorized";
        case AuthenticationError::SaslTemporaryAuthFailure: return "SaslTemporaryAuthFailure";
        case AuthenticationError::NonSaslConflict: return "NonSaslConflict";
        case AuthenticationError::NonSaslNotAcceptable: return "NonSaslNotAcceptable";
        case AuthenticationError::NonSaslNotAuthorized: return "NonSaslNotAuthorized";
    }

    return "unknown error";
}
