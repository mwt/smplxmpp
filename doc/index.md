\mainpage

# smplxmpp developer Documentation
Welcome to the developer documentation for `smplxmpp`!

If you are looking for a place to start reading, I suggest [Architecture](architecture.md) from the *Related Pages*.

## Content
This contains lists and descriptions of all internally used APIs.
Note that the source code for both binary files is not contained here, as they only contain a `main()` function and do not provide any APIs themselves.


Some additional Issues are discussed in the *Related Pages* (view their source at `doc/`),
for example the build system.

## Other Documentation
Please refer to additional documentation, which, unfourtunately, can't be linked from here:

- the man pages in `man/` (view with `man ./man/smplxmpp.1.in` e.g.)
- the readme file `README.md`
