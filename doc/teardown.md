# Connection Teardown (Teardown Handshake)
Due to a quirk in the gloox api, the connection teardown is rather unconventional.

The scenario is that some message will be piped to smplxmpp.
The IO thread reads the message and enqueues it, after that reads EOF and communicates "stop" to the connection handling thread by calling `SmplXmppHandlerBase::stop()`.

If `SmplXmppHandlerBase` would then call `Client::disconnect()` (gloox api) the connection would be immedeately terminated, even before the message has been sent.

Waiting for the message queue to be empty does not work either:
The smplxmpp message queue will be empty after the message has been submitted to the gloox api.
But that **does not mean** that the message has been sent:
Gloox will in turn enqueue the message internally for sending later.
If smplxmpp were to now call `Client::disconnect()` (gloox api), the connection would be terminated immedeately and the message would not be sent to the server, even though the smplxmpp message queue is empty.

> **THE LOGS DO NOT DISPLAY THIS. THEY WILL SHOW A STANZA HAS BEEN SENT BEFORE IT HAS LEFT THE INTERNAL GLOOX QUEUE.**

So smplxmpp actually should wait for the internal gloox queue to be empty.
But this information is not exposed by gloox.
To ensure the message stanza has left the gloox queue, smplxmpp sends another stanza via gloox **after** the message has been sent.
As soon as a response to this second stanza is received, the message stanza must have left the queue.

> If at one day gloox stops using a queue internally and switches to delivering stanzas independently of each other, this method will fail.

This ping-pong of the second stanza and its response is referred to as a "teardown handshake".
It sends an empty IQ stanza with a random ID to the server -- which is not a valid stanza.
According to [RFC 6120, section 8.3.1](https://www.rfc-editor.org/rfc/rfc6120.html#section-8.3.1), errornous IQs **must** be responded to with an error.

As soon as an error for the sent IQ stanza (identified by it's ID) is received, the teardown handshake is completed.

After the teardown handshake is done, `Client::disconnect()` (gloox api) will be called and the connection terminated.

> This is complicated.
> But the best solution i could come up with without taking apart gloox internals.
