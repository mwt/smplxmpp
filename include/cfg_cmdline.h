#ifndef __SMPLXMPP_CFG_CMDLINE_H_INCLUDED__
#define __SMPLXMPP_CFG_CMDLINE_H_INCLUDED__

#include <argp.h>
#include <string>
#include <fstream>

#include <config.h>
#include <xmpp.h>

using namespace std;

/**
 * holds all configuration information
 */
struct SmplXmppGlobalOptions {
    string jid;
    string pass;
    SmplXmppHandlerConfig cfg;
};

extern struct SmplXmppGlobalOptions opts;

extern const char* argp_program_bug_address;
extern void (*argp_program_version_hook)(FILE*, struct argp_state*);

/**
 * command line arguments accepted by smplxmpp
 */
extern const struct argp_option options[];

/**
 * parse a single command line option
 * called by argp
 * @param key current option
 * @param arg value of the option
 * @param state current state of the parse
 */
int parse_opt(int key, char* arg, struct argp_state* state);

/**
 * prints the version of smplxmpp & gloox.
 * conforms to argp interface
 */
void smplxmppPrintVersion(FILE*, struct argp_state*);

#endif // __SMPLXMPP_CFG_CMDLINE_H_INCLUDED__
